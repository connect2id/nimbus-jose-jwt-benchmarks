Nimbus JOSE+JWT Benchmarks


Simple benchmarks for measuring the performance of selected JWS and JWE 
algorithm implementations from the Nimbus JOSE+JWT library for Java.

Requires Maven to build.

To post bug reports and suggestions

https://bitbucket.org/nimbusds/nimbus-jose-jwt-benchmarks/issues


Follow us on Twitter

https://twitter.com/NimbusDS


[EOF]
