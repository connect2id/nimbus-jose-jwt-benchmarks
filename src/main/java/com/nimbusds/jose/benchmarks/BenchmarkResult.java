package com.nimbusds.jose.benchmarks;


/**
 * Encapsulates the result of a benchmark run.
 */
public class BenchmarkResult {
	
	
	/**
	 * The benchmark iterations.
	 */
	private final int iterations;
	
	
	/**
	 * The benchmark runtime, in nanoseconds.
	 */
	private final long runtime;
	
	
	/**
	 * The benchmark log.
	 */
	private final String log;
	
	
	/**
	 * Creates a new benchmark result.
	 * 
	 * @param iterations The benchmark total iteration count.
	 * @param runtime    The benchmark total runtime, in nanoseconds.
	 * @param log        Optional message log, {@code null} if none.
	 */
	public BenchmarkResult(final int iterations, final long runtime, final String log) {
		
		this.iterations = iterations;
		this.runtime = runtime;
		this.log = log;
	}

	
	/**
	 * Gets the benchmark total iteration count.
	 * 
	 * @return The iteration count.
	 */
	public int getIterations() {
		return iterations;
	}

	
	/**
	 * Gets the benchmark total runtime.
	 * 
	 * @return The total runtime, in nanoseconds.
	 */
	public long getRuntime() {
		return runtime;
	}

	
	/**
	 * Gets the optional message log.
	 * 
	 * @return The message log, {@code null} if none.
	 */
	public String getLog() {
		return log;
	}
}
