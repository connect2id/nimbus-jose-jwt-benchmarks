package com.nimbusds.jose.benchmarks;


import java.math.BigInteger;


/**
 * Example Elliptic Curve key, copied from the JWS specification. This class is
 * immutable.
 */
public final class ExampleECKey {
	
	public final static byte[] X_BYTES = { 
		(byte) 127, (byte) 205, (byte) 206, (byte)  39, 
		(byte) 112, (byte) 246, (byte) 196, (byte)  93, 
		(byte)  65, (byte) 131, (byte) 203, (byte) 238, 
		(byte) 111, (byte) 219, (byte)  75, (byte) 123, 
		(byte)  88, (byte)   7, (byte)  51, (byte)  53, 
		(byte) 123, (byte) 233, (byte) 239, (byte)  19, 
		(byte) 186, (byte) 207, (byte) 110, (byte)  60, 
		(byte) 123, (byte) 209, (byte)  84, (byte)  69 };


	public final static byte[] Y_BYTES = { 
		(byte) 199, (byte) 241, (byte)  68, (byte) 205, 
		(byte)  27, (byte) 189, (byte) 155, (byte) 126, 
		(byte) 135, (byte) 44,  (byte) 223, (byte) 237, 
		(byte) 185, (byte) 238, (byte) 185, (byte) 244, 
		(byte) 179, (byte) 105, (byte)  93, (byte) 110, 
		(byte) 169, (byte)  11, (byte)  36, (byte) 173, 
		(byte) 138, (byte)  70, (byte)  35, (byte)  40, 
		(byte) 133, (byte) 136, (byte) 229, (byte) 173 };


	public final static byte[] D_BYTES = { 
		(byte) 142, (byte) 155, (byte)  16, (byte) 158, 
		(byte) 113, (byte) 144, (byte) 152, (byte) 191, 
		(byte) 152, (byte)   4, (byte) 135, (byte) 223, 
		(byte)  31, (byte)  93, (byte) 119, (byte) 233, 
		(byte) 203, (byte)  41, (byte)  96, (byte) 110, 
		(byte) 190, (byte) 210, (byte)  38, (byte)  59, 
		(byte)  95, (byte)  87, (byte) 194, (byte)  19, 
		(byte) 223, (byte) 132, (byte) 244, (byte) 178 };
	
	
	/**
	 * The public 'x' parameter.
	 */
	public final static BigInteger X = new BigInteger(1, X_BYTES);
	
	
	/**
	 * The public 'y' parameter.
	 */
	public final static BigInteger Y = new BigInteger(1, Y_BYTES);
	
	
	/**
	 * The private 'd' parameter.
	 */
	public final static BigInteger D = new BigInteger(1, D_BYTES);
	
	
	/**
	 * Prevents public instantiation.
	 */
	private ExampleECKey() {}
}
