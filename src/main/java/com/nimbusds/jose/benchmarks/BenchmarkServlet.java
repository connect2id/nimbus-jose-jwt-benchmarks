package com.nimbusds.jose.benchmarks;


import com.nimbusds.jose.JWSAlgorithm;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet for running the configured JOSE benchmarks.
 */
public class BenchmarkServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
		throws ServletException, IOException {
		
		resp.addHeader("Content-Type", "text/plain;utf-8");
		
		PrintWriter out = resp.getWriter();
		
		out.println("*** JWS Benchmarks ***\n\n\n");
		
		try {
			out.println("*** Running JWS RS256 verification benchmark ***");
			BenchmarkResult result = JWSBenchmark.runRSASSAVerificationBenchmark(JWSAlgorithm.RS256);
			out.println(result.getLog());
			out.println( "\n\n\n");
			
			out.println("*** Running JWS RS384 verification benchmark ***");
			result = JWSBenchmark.runRSASSAVerificationBenchmark(JWSAlgorithm.RS384);
			out.println(result.getLog());
			out.println("\n\n\n");
			
			out.println("*** Running JWS RS512 verification benchmark ***");
			result = JWSBenchmark.runRSASSAVerificationBenchmark(JWSAlgorithm.RS512);
			out.println(result.getLog());
			out.println("\n\n\n");

		} catch (Exception e) {

			throw new ServletException("Benchmark exception: " + e.getMessage(), e);
		}
	}
}
