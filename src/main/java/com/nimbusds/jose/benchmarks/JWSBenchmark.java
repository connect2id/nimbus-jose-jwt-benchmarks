package com.nimbusds.jose.benchmarks;


import java.security.*;
import java.security.interfaces.*;
import java.util.*;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.*;
import com.nimbusds.jwt.*;


/**
 * JWS benchmark for RSA-SSA signature verification, using 1024-bit keys.
 */
public class JWSBenchmark {

	
	private final static int ITERATIONS = 1000;
	

	/**
	 * Composes a new example JSON Web Token (JWT) claims set.
	 *
	 * <pre>
	 * {
	 * "iss"   : "https://c2id.com/op",
	 * "iat"   : 1370598200,
	 * "exp"   : 1370600000,
	 * "sub"   : "alice@wonderland.net",
	 * "scope" : "openid profile email webapp:post webapp:browse",
	 * "aud"   : ["http://webapp.com/rest/v1", "http://webapp.com/rest/v2"]
	 * }
	 * </pre>
	 *
	 * @return The JWT claims set.
	 */
	public static JWTClaimsSet composeJWTClaimsSet() {
		
		JWTClaimsSet claims = new JWTClaimsSet();
		
		claims.setIssuer("https://c2id.com/op");
		claims.setSubject("alice@wonderland.net");

		List<String> aud = new LinkedList<String>();
		aud.add("http://webapp.com/rest/v1");
		aud.add("http://webapp.com/rest/v2");
		claims.setAudience(aud);

		claims.setIssueTime(new Date());
		claims.setExpirationTime(new Date(new Date().getTime() + (30*60*1000)));

		claims.setCustomClaim("scope", "openid profile email webapp:post webapp:browse");
		
		return claims;
	}
	

	/**
	 * Runs an RSA-SSA signing benchmark.
	 * 
	 * @param alg The JWS algorithm, must be RS256, RS384 or RS512. Must
	 *            not be {@code null}.
	 * 
	 * @return The benchmark result.
	 * 
	 * @throws Exception If an exception was encountered.
	 */
	public static BenchmarkResult runRSASSASigningBenchmark(final JWSAlgorithm alg)
		throws Exception {
		
		StringBuilder log = new StringBuilder();
		
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
                final int keyBitSize = 1024;
		keyGen.initialize(keyBitSize);
		KeyPair keyPair = keyGen.genKeyPair();

		RSAPrivateKey priv = (RSAPrivateKey)keyPair.getPrivate();
		RSAPublicKey pub = (RSAPublicKey)keyPair.getPublic();

		log.append("Generated " + keyBitSize + "-bit RSA key pair\n");

		RSASSASigner signer = new RSASSASigner(priv);

		log.append("Created RSA-SSA signer with private RSA key\n");
		
		JWTClaimsSet claims = composeJWTClaimsSet();
		log.append("JWT claims: " + claims.toJSONObject() + "\n");

		JWSHeader header = new JWSHeader(alg);
		
		// Warm up
		log.append("Warming up the benchmark...\n");
		
		for (int i=0; i < ITERATIONS; i++) {
                    
                        SignedJWT jwt = new SignedJWT(header, claims);
			jwt.sign(signer);
		}


		// Actual measurement
		
		log.append("Running " + alg + " signing benchmark...\n");
		
		long startTime = System.nanoTime();

		for (int i=0; i < ITERATIONS; i++) {

                        SignedJWT jwt = new SignedJWT(header, claims);
			jwt.sign(signer);
		}

		long endTime = System.nanoTime();

		long runTime = endTime - startTime;

		log.append("Total benchmark runtime (" + ITERATIONS + " iterations): " + runTime + "ns\n");

		long validateTime = runTime / ITERATIONS;

		log.append("JWS " + alg + " signing operation: " + validateTime + "ns\n");
		log.append("JWS " + alg + " signing operation: " + validateTime / 1000 + "us\n");
		
		return new BenchmarkResult(ITERATIONS, runTime, log.toString());
	}
	
	
	/**
	 * Runs an RSA-SSA signature verification benchmark.
	 * 
	 * @param alg The JWS algorithm, must be RS256, RS384 or RS512. Must
	 *            not be {@code null}.
	 * 
	 * @return The benchmark result.
	 * 
	 * @throws Exception If an exception was encountered.
	 */
	public static BenchmarkResult runRSASSAVerificationBenchmark(final JWSAlgorithm alg)
		throws Exception {
		
		StringBuilder log = new StringBuilder();
		
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
                final int keyBitSize = 1024;
		keyGen.initialize(keyBitSize);
		KeyPair keyPair = keyGen.genKeyPair();

		RSAPrivateKey priv = (RSAPrivateKey)keyPair.getPrivate();
		RSAPublicKey pub = (RSAPublicKey)keyPair.getPublic();

		log.append("Generated " + keyBitSize + "-bit RSA key pair\n");

		RSASSASigner signer = new RSASSASigner(priv);

		log.append("Created RSA-SSA signer with private RSA key\n");
		
		JWTClaimsSet claims = composeJWTClaimsSet();
		log.append("JWT claims: " + claims.toJSONObject() + "\n");

		JWSHeader header = new JWSHeader(alg);
		SignedJWT jwt = new SignedJWT(header, claims);
		jwt.sign(signer);

		log.append("Signed JWT with " + alg + "\n");

		String jwtString = jwt.serialize();

		log.append("Resulting JWT [" + jwtString.length() + " chars]: " + jwtString + "\n");


		RSASSAVerifier verifier = new RSASSAVerifier(pub);
		
		log.append("Created RSA-SSA verifier with public RSA key\n");


		boolean validSignature = jwt.verify(verifier);

		log.append("The JWS signature is verified: " + validSignature + "\n");


		// Warm up
		log.append("Warming up the benchmark...\n");
		
		for (int i=0; i < ITERATIONS; i++) {

			jwt.verify(verifier);
		}


		// Actual measurement
		
		log.append("Running " + alg + " signature verification benchmark...\n");
		
		long startTime = System.nanoTime();

		for (int i=0; i < ITERATIONS; i++) {

			jwt.verify(verifier);
		}

		long endTime = System.nanoTime();

		long runTime = endTime - startTime;

		log.append("Total benchmark runtime (" + ITERATIONS + " iterations): " + runTime + "ns\n");

		long validateTime = runTime / ITERATIONS;

		log.append("JWS " + alg + " verification per signature: " + validateTime + "ns\n");
		log.append("JWS " + alg + " verification per signature: " + validateTime / 1000 + "us\n");
		
		return new BenchmarkResult(ITERATIONS, runTime, log.toString());
	}
	
	
	/**
	 * Runs an EC-DSA signing benchmark.
	 * 
	 * @param alg The JWS algorithm, must be ES256, ES384 or ES512. Must
	 *            not be {@code null}.
	 * 
	 * @return The benchmark result.
	 * 
	 * @throws Exception If an exception was encountered.
	 */
	public static BenchmarkResult runECDSASigningBenchmark(final JWSAlgorithm alg)
		throws Exception {
		
		StringBuilder log = new StringBuilder();
		
		ECDSASigner signer = new ECDSASigner(ExampleECKey.D);

		log.append("Created EC-DSA signer with private D parameter " + ExampleECKey.D + " \n");
		
		JWTClaimsSet claims = composeJWTClaimsSet();
		log.append("JWT claims: " + claims.toJSONObject() + "\n");

		JWSHeader header = new JWSHeader(alg);

		// Warm up
		log.append("Warming up the benchmark...\n");
		
		for (int i=0; i < ITERATIONS; i++) {
                    
                        SignedJWT jwt = new SignedJWT(header, claims);
			jwt.sign(signer);
		}


		// Actual measurement
		
		log.append("Running " + alg + " signing benchmark...\n");
		
		long startTime = System.nanoTime();

		for (int i=0; i < ITERATIONS; i++) {

                        SignedJWT jwt = new SignedJWT(header, claims);
			jwt.sign(signer);
		}

		long endTime = System.nanoTime();

		long runTime = endTime - startTime;

		log.append("Total benchmark runtime (" + ITERATIONS + " iterations): " + runTime + "ns\n");

		long validateTime = runTime / ITERATIONS;

		log.append("JWS " + alg + " signing operation: " + validateTime + "ns\n");
		log.append("JWS " + alg + " signing operation: " + validateTime / 1000 + "us\n");
		
		return new BenchmarkResult(ITERATIONS, runTime, log.toString());
	}
	
	
	/**
	 * Runs an EC-DSA signature verification benchmark.
	 * 
	 * @param alg The JWS algorithm, must be ES256, ES384 or ES512. Must
	 *            not be {@code null}.
	 * 
	 * @return The benchmark result.
	 * 
	 * @throws Exception If an exception was encountered.
	 */
	public static BenchmarkResult runECDSAVerificationBenchmark(final JWSAlgorithm alg)
		throws Exception {
		
		StringBuilder log = new StringBuilder();
		
		ECDSASigner signer = new ECDSASigner(ExampleECKey.D);

		log.append("Created EC-DSA signer with private D parameter " + ExampleECKey.D + " \n");
		
		JWTClaimsSet claims = composeJWTClaimsSet();
		log.append("JWT claims: " + claims.toJSONObject() + "\n");

		JWSHeader header = new JWSHeader(alg);
		SignedJWT jwt = new SignedJWT(header, claims);
		jwt.sign(signer);

		log.append("Signed JWT with " + alg + "\n");

		String jwtString = jwt.serialize();

		log.append("Resulting JWT [" + jwtString.length() + " chars]: " + jwtString + "\n");


		ECDSAVerifier verifier = new ECDSAVerifier(ExampleECKey.X, ExampleECKey.Y);
		
		log.append("Created EC-DSA verifier with X and Y parameters\n");


		boolean validSignature = jwt.verify(verifier);

		log.append("The JWS signature is verified: " + validSignature + "\n");


		// Warm up
		log.append("Warming up the benchmark...\n");
		
		for (int i=0; i < ITERATIONS; i++) {

			jwt.verify(verifier);
		}


		// Actual measurement
		
		log.append("Running " + alg + " signature verification benchmark...\n");
		
		long startTime = System.nanoTime();

		for (int i=0; i < ITERATIONS; i++) {

			jwt.verify(verifier);
		}

		long endTime = System.nanoTime();

		long runTime = endTime - startTime;

		log.append("Total benchmark runtime (" + ITERATIONS + " iterations): " + runTime + "ns\n");

		long validateTime = runTime / ITERATIONS;

		log.append("JWS " + alg + " verification per signature: " + validateTime + "ns\n");
		log.append("JWS " + alg + " verification per signature: " + validateTime / 1000 + "us\n");
		
		return new BenchmarkResult(ITERATIONS, runTime, log.toString());
	}
	
	
	/**
	 * Runs the benchmarks in this class.
	 * 
	 * @param args No command line arguments are expected.
	 * 
	 * @throws Exception 
	 */
	public static void main(String args[])
		throws Exception {
		
		System.out.println("*** Running JWS RS256 signing benchmark ***");
		BenchmarkResult result = runRSASSASigningBenchmark(JWSAlgorithm.RS256);                     
                System.out.println(result.getLog());
		
//		System.out.println("*** Running JWS RS384 signing benchmark ***");
//		result = runRSASSASigningBenchmark(JWSAlgorithm.RS384);
//		System.out.println(result.getLog());
//		
//		System.out.println("*** Running JWS RS512 signing benchmark ***");
//		result = runRSASSASigningBenchmark(JWSAlgorithm.RS512);
//		System.out.println(result.getLog());
		
//		System.out.println("*** Running JWS ES256 signing benchmark ***");
//		result = runECDSASigningBenchmark(JWSAlgorithm.ES256);
//               System.out.println(result.getLog());

		System.out.println("*** Running JWS RS256 verification benchmark ***");
		result = runRSASSAVerificationBenchmark(JWSAlgorithm.RS256);
		System.out.println(result.getLog());
		
//		System.out.println("*** Running JWS RS384 verification benchmark ***");
//		result = runRSASSAVerificationBenchmark(JWSAlgorithm.RS384);
//		System.out.println(result.getLog());
//		
//		System.out.println("*** Running JWS RS512 verification benchmark ***");
//		result = runRSASSAVerificationBenchmark(JWSAlgorithm.RS512);
//		System.out.println(result.getLog());
		
//		System.out.println("*** Running JWS ES256 verification benchmark ***");
//		result = runECDSAVerificationBenchmark(JWSAlgorithm.ES256);
//		System.out.println(result.getLog());
	}
}